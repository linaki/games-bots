import pyautogui as g
from time import sleep
import random
from pynput import mouse


actualBlock=(334, 511)
blocksWin=((670, 100), (882, 947))
running=True

MAXSCROLL=300

def randomBlock(screen):
    xy1, xy2=blocksWin
    x1, y1=xy1
    x2, y2=xy2
    x=random.randint(x1, x2)
    y = random.randint(y1, y2)
    print(screen.getpixel((x, y)))
    if screen.getpixel((x, y)) == (255, 255, 255):
        print("Recomputing move")
        return randomBlock(screen)
    return x, y
print("Starting InfiniteBot")
while running:
    screen = g.screenshot()
    g.moveTo(randomBlock(screen))
    sleep(0.1)
    g.scroll(random.randint(-MAXSCROLL, MAXSCROLL))
    sleep(0.3)
    screen=g.screenshot()
    g.moveTo(randomBlock(screen))
    sleep(0.1)
    g.dragTo(*actualBlock, 1)
    with mouse.Events() as events:
        event=events.get(0.5)
        if not event:
            continue
        else:
            print("Quitting")
            break