import requests
from bs4 import BeautifulSoup
import threading

ranking=[]
def words():
    f=open("words.txt")
    while True:
        yield f.readline().replace("\n", "")
def getScore(word, id):
    payload={"word" : word.lower()}
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:126.0) Gecko/20100101 Firefox/126.0',
               'referer' : 'https://cemantix.certitudes.org/',
               'origin' : 'https://cemantix.certitudes.org'}
    r=requests.post("https://cemantix.certitudes.org/score", data=payload, headers=headers)
    response=r.json()

    if "error" in response:
        del threads[id]
        return
    elif response["score"]==1:
        print("I think I found the word !!!!!!!")
        print(word)
        exit()
    else:
        if id%100==0:
            ranking.sort(reverse=True)
            print("New best words ranking")
            print("\n       ".join(word+" : "+str(score) for score, word in ranking[:20]))
        ranking.append((response["score"]*100, word.lower()))
    del threads[id]

threads={}
pos=0
for word in words():
    threads[pos]=(threading.Thread(target=getScore, args=(word,pos)))
    threads[pos].start()
    pos+=1
